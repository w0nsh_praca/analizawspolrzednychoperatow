﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Windows.Forms;

namespace AnalizaWspolrzednychOperatow.Logic
{
    public class Settings
    {
        public Settings() { }
        public Settings(string json_path)
        {
            using (FileStream file = new FileStream(json_path, FileMode.Open, FileAccess.Read))
            using (StreamReader reader = new StreamReader(file))
            {
                JObject token = JObject.Parse(reader.ReadToEnd());
                this.DefaultSystem = token["uklad_default"].ToString();
                foreach(JProperty sys in ((JObject)token["uklad"]).Properties())
                    Systems[sys.Name] = new Range((int)sys.Value["x"][0], (int)sys.Value["x"][1], 
                                                (int)sys.Value["y"][0], (int)sys.Value["y"][1]);
            }
        }
        public string DefaultSystem = "";
        public Dictionary<string, Range> Systems = new Dictionary<string, Range>();
        
        public struct Range
        {
            public int X1, X2, Y1, Y2;
            public Range(int x1, int x2, int y1, int y2)
            {
                this.X1 = x1; this.X2 = x2;
                this.Y1 = y1; this.Y2 = y2;
            }
        }
    }
}
