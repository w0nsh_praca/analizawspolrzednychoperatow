﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Data;
using System.Data.OleDb;
using ClosedXML.Excel;
using System.Windows.Forms;

namespace AnalizaWspolrzednychOperatow.Logic
{
    public static class Analyze
    {
        public static void Run(List<string> files, Settings settings, Action<string, Color> report_log, Action<string, Color> error_log, Action<int> report_progress)
        {
            report_log("Analiza " + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + "\n", Color.Navy);
            report_log("Liczba plików: " + files.Count.ToString() + "\n", Color.Navy);
            int done = 0;
            foreach (string file in files)
            {
                AnalyzeReport report = analyze_one(file, settings, error_log);
                report_log("Analizowanie ", Color.Navy);
                report_log(file, Color.DimGray);
                report_log(":\n", Color.Navy);
                foreach (string msg in report.Messages) report_log("    " + msg + "\n", Color.DarkCyan); 
                if (report.Errors.Count > 0) {
                    error_log("Błędy w pliku ", Color.Navy);
                    error_log(file, Color.DimGray);
                    error_log(":\n", Color.Navy);
                }
                foreach (string warning in report.Warnings) error_log("   " + warning + "\n", Color.Brown);
                foreach (string error in report.Errors) error_log("    " + error + "\n", Color.DarkRed);
                done++;
                report_progress((done * 100) / files.Count);
            }
            report_log("Analiza zakończona", Color.Navy);
        }

        private static object ToValue(IXLCell cell)
        {
            if (cell.HasFormula) return cell.CachedValue;
            return cell.Value;
        }

        public static bool CheckCords(IXLWorksheet workSheet, Settings.Range range, int nextRow, int xyCell)
        {
            if ((Int32.TryParse(workSheet.Row(nextRow).Cell(xyCell).Value.ToString().Substring(0, 4), out int cell)))
            {
                if (xyCell == 2)
                    if (cell >= range.X1 && cell <= range.X2) return true;
                if (xyCell == 3)
                    if (cell >= range.Y1 && cell <= range.X2) return true;
            }            
            return false;     
        }

        public static string ReadCords(IXLWorksheet workSheet, Settings settings, AnalyzeReport report)
        {
            report.Messages.Add("Analiza plików - krok 4 - uproszczona analiza układu współrzędnych");
            string uklad = "";                     
            if ((Int32.TryParse(workSheet.Row(1).Cell(2).Value.ToString().Substring(0, 4), out int x))
                && (Int32.TryParse(workSheet.Row(1).Cell(3).Value.ToString().Substring(0, 4), out int y)))
            {              
                    foreach (var kvp in settings.Systems)
                    {
                        if (x >= kvp.Value.X1 && x <= kvp.Value.X2 && y >= kvp.Value.Y1 && y <= kvp.Value.Y2)
                        {
                            uklad = kvp.Key;
                            break;
                        }
                    }
                    if (uklad == "") uklad = settings.DefaultSystem;                                          
            }
            return uklad;
        }

        public static bool CheckXY(IXLWorksheet workSheet, Action<string, Color> debug, string cordsChecked, Settings settings, AnalyzeReport report)
        {
            report.Messages.Add("Analiza plików - krok 5 - analiza zawartości kolumn");
            int counterX = 1;
            int counterY = 1;
            int turnXY = 1;

            if (cordsChecked != "Lokalny")
            {
                int lastrow = workSheet.LastRowUsed().RowNumber();
                var rows = workSheet.Rows(1, lastrow);

                foreach (IXLRow row in rows)
                {
                    foreach (IXLCell cell in row.Cells(2, 3))
                    {
                        int xyCell = 0;
                        if (cell.IsEmpty())
                        {
                            if (turnXY == 1)
                            {
                                cell.Style.Fill.BackgroundColor = XLColor.Yellow;
                                report.Errors.Add("W kolumnie " + 2 + ", wierszu " + counterX + " znaleziono pusta komorke\n");
                                turnXY++; counterX++;                               
                            }
                            else if (turnXY >= 2)
                            {
                                cell.Style.Fill.BackgroundColor = XLColor.Yellow;
                                report.Errors.Add("W kolumnie " + 3 + ", wierszu " + counterY + " znaleziono pusta komorke\n");
                                turnXY = 1; counterY++;                               
                            }
                        }
                        else
                        {
                            if (turnXY == 1)
                            {
                                xyCell = 2;
                                if (CheckCords(workSheet, settings.Systems[cordsChecked], counterX, xyCell) == false)
                                {
                                    cell.Style.Fill.BackgroundColor = XLColor.Orange;
                                    report.Errors.Add("W kolumnie " + 2 + ", w wierszu " + counterX + " znaleziono niepoprawny numer\n");
                                }

                                counterX++;
                            }

                            if (turnXY == 2)
                            {
                                //MessageBox.Show("y: " + cell.Value.ToString());
                                xyCell = 3;
                                if (CheckCords(workSheet, settings.Systems[cordsChecked], counterY, xyCell) == false)
                                {
                                    //debug("W kolumnie " + 3 + ", w wierszu " + counterY + " znaleziono niepoprawny numer\n", Color.Aqua);
                                    cell.Style.Fill.BackgroundColor = XLColor.Orange;
                                    report.Errors.Add("W kolumnie " + 3 + ", w wierszu " + counterY + " znaleziono niepoprawny numer\n");
                                }

                                counterY++;
                            }
                            turnXY++;
                            if (turnXY > 2) turnXY = 1;
                        }
                    }
                }
            }
            return true;
        }

        public static bool CheckDate(IXLWorksheet ws, Action<string, Color> debug, AnalyzeReport report)
        {            
            DateTime date;
            int lastrow = ws.LastRowUsed().RowNumber();
            var rows = ws.Rows(1, lastrow);
            int counter = 1;
            foreach (IXLRow row in ws.Rows(1, lastrow))
            {
                foreach (IXLCell cell in row.Cells(5, 5))
                {
                    if (cell.IsEmpty())
                    {
                        cell.Style.Fill.BackgroundColor = XLColor.Yellow;
                        report.Errors.Add("W kolumnie " + 5 + ", wierszu " + counter + ", brakuje daty\n");                      
                    }
                        

                    else if (!cell.TryGetValue(out date))
                    {
                        cell.Style.Fill.BackgroundColor = XLColor.Orange;
                        report.Errors.Add("W kolumnie " + 5 + ", wierszu " + counter + ", data ma niewlasciwy format\n");
                    }
                    counter++;
                }
            }
            return false;
        }

        public static bool CheckKERG(IXLWorksheet ws, Action<string, Color> debug, AnalyzeReport report)
        {
            report.Messages.Add("Analiza plików - krok 6 - uzupełnienie i analiza numeru operatu");
            int lastrow = ws.LastRowUsed().RowNumber();
            var rows = ws.Rows(1, lastrow);
            int counter = 1;
            foreach (IXLRow row in ws.Rows(1, lastrow))
            {
                foreach (IXLCell cell in row.Cells(6, 6))
                {
                    if (cell.IsEmpty())
                    {
                        cell.Style.Fill.BackgroundColor = XLColor.Yellow;
                        report.Errors.Add("W kolumnie " + 6 + ", wierszu " + counter + ", brakuje numeru KERG\n");                       
                    }
                    counter++;
                }
            }
                return false;
        }

        public static void CompareFileName(string lineup, string path, AnalyzeReport report)
        {
            string filename = System.IO.Path.GetFileName(path);
            if (filename == (lineup + ".xlsx")) report.Messages.Add("INFO: Nazwa pliku się zgadza");
            else
            {
                report.Errors.Add("Nazwa pliku się nie zgadza\n");
                report.Messages.Add("INFO: Zmieniono nazwę pliku na " + lineup + ".xls");
            }
        }      

        public static void DelColumns(IXLWorksheet workSheet, int colNr, AnalyzeReport report)
        {
            if(colNr == 2)
                report.Messages.Add("Analiza plików - krok 2 - analiza pustych kolumn przed kolumną X");
            if(colNr == 3)
                report.Messages.Add("Analiza plików - krok 2 - analiza pustych kolumn przed kolumną Y");

            int delCols = 0;
            for (; colNr < workSheet.LastColumnUsed().ColumnNumber(); colNr++)
            {
                if (!workSheet.Column(colNr).IsEmpty()) break;
                if (workSheet.Column(colNr).IsEmpty())
                {
                    workSheet.Column(colNr).Delete();
                    delCols++;
                    colNr--;
                }
            }
            report.Messages.Add("INFO: Usunięto " + delCols + " puste kolumny");
        }

        private static AnalyzeReport analyze_one(string path, Settings settings, Action<string, Color> debug)
        {
            AnalyzeReport report = new AnalyzeReport();
            var workBook = new XLWorkbook(path);
            IXLWorksheet workSheet = workBook.Worksheet(1);

            //kontrola1
            report.Messages.Add("Analiza plików - krok 1 - analiza nagłówków");
            if (workSheet.Row(1).Cell(1).Value.ToString() == "NR")
            {
                workSheet.Row(1).Delete();
                report.Messages.Add("INFO: Usunięto nagłówek");
            }

            //kontrola2
            DelColumns(workSheet, 2, report);

            //kontrola3
            DelColumns(workSheet, 3, report);

            //kontrola4
            string cordsChecked = ReadCords(workSheet, settings, report);
            CompareFileName(cordsChecked, path, report);

            //kontrola5
            if (workSheet.Column(1).IsEmpty()) report.Errors.Add("Kolumna NR jest pusta\n");
            if (workSheet.Column(2).IsEmpty()) report.Errors.Add("Kolumna X jest pusta\n");
            if (workSheet.Column(3).IsEmpty()) report.Errors.Add("Kolumna Y jest pusta\n");
            if (workSheet.Column(5).IsEmpty()) report.Errors.Add("Kolumna DTP jest pusta\n");

            CheckXY(workSheet, debug, cordsChecked, settings, report);
            CheckDate(workSheet, debug, report);

            //kontrola6
            CheckKERG(workSheet, debug, report);
            workBook.SaveAs(@"D:\projektyVS\analizawspolrzednychoperatow\excels\test\" + cordsChecked +".xlsx");

            //wypisanie
            //foreach (IXLRow row in workSheet.Rows())
            //{
            //    string log = "";
            //    foreach(IXLCell cell in row.Cells())
            //    {
            //        log += cell.Value.ToString() + ", ";
            //    }
            //    log += "\n";
                
            //    debug(log, Color.DarkGreen);
            //}                               

            return report;
        }

        public class AnalyzeReport
        {
            public List<string> Errors = new List<string>();
            public List<string> Warnings = new List<string>();
            public List<string> Messages = new List<string>();
        }
    }
}
