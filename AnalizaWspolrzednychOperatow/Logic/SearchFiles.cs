﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalizaWspolrzednychOperatow.Logic
{
    public static class SearchFiles
    {
        public static List<string> SearchRecursive(string path)
        {
            List<string> ret = new List<string>();
            Stack<string> dirs = new Stack<string>();
            dirs.Push(path);
            while(dirs.Count > 0)
            {
                string cur = dirs.Pop();
                get_all(cur, ref ret);
                foreach (string d in Directory.GetDirectories(cur)) dirs.Push(d);
            }
            return ret;
        }

        public static List<string> Search(string path)
        {
            List<string> ret = new List<string>();
            get_all(path, ref ret);
            return ret;
        }

        private static void get_all(string path, ref List<string> list)
        {
            foreach (string f in Directory.GetFiles(path))
            {
                if (f.EndsWith(".xlsx")) list.Add(f);
            }
        }
    }
}
