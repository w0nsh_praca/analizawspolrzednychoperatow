﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.WindowsAPICodePack.Dialogs;
using AnalizaWspolrzednychOperatow.Logic;

namespace AnalizaWspolrzednychOperatow
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.locationTextBox.Text = @"D:\projektyVS\analizawspolrzednychoperatow\excels\test";
            this.settingsTextBox.Text = @"D:\projektyVS\analizawspolrzednychoperatow\data\ustawienia.json";
        }

        private void executeButton_Click(object sender, EventArgs e)
        {
            // Empty textboxes
            if(this.locationTextBox.Text.Length == 0 || this.settingsTextBox.Text.Length == 0)
            {
                MessageBox.Show("Podaj lokalizację oraz ścieżkę do pliku z ustawieniami", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // Settigns
            Settings settings;
            try
            {
                settings = new Settings(this.settingsTextBox.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Błąd przy wczytywaniu ustawień:\n\n" + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // Errors
            List<string> dirs;
            try
            {
                dirs = (this.recursiveCheckBox.Checked ? SearchFiles.SearchRecursive(this.locationTextBox.Text) :
                                                                    SearchFiles.Search(this.locationTextBox.Text));
            }
            catch(Exception ex)
            {
                MessageBox.Show("Błąd przy szukaniu plików:\n\n" + ex.Message, "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            // Are you sure?
            if(MessageBox.Show("Wykonać program dla " + dirs.Count.ToString() + " plików?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                return;
            // Worker
            this.progressBar.Value = 0;
            this.reportRichTextBox.Text = "";
            this.errorRichTextBox.Text = "";
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = true;
            // on completion
            worker.RunWorkerCompleted += (object se, RunWorkerCompletedEventArgs ev) =>
            {
                if (ev.Error != null)
                {
                    MessageBox.Show("Podczas transferu wystąpił błąd:\n\n" + ev.Error.Message,
                        "Błąd", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Transfer zakończony.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.progressBar.Value = 0;
                this.executeButton.Enabled = true;
            };
            worker.ProgressChanged += (object se, ProgressChangedEventArgs ev) =>
            {
                this.progressBar.Value = ev.ProgressPercentage;
            };
            Action<string, Color> report_log = (string str, Color color) =>
            {
                RichTextBox text_box = this.reportRichTextBox;
                text_box.Invoke((MethodInvoker)delegate
                {
                    text_box.SelectionStart = text_box.TextLength;
                    text_box.SelectionLength = 0;
                    text_box.SelectionColor = color;
                    text_box.AppendText(str);
                    text_box.SelectionColor = text_box.ForeColor;
                });
            };
            Action<string, Color> error_log = (string str, Color color) =>
            {
                RichTextBox text_box = this.errorRichTextBox;
                text_box.Invoke((MethodInvoker)delegate
                {
                    text_box.SelectionStart = text_box.TextLength;
                    text_box.SelectionLength = 0;
                    text_box.SelectionColor = color;
                    text_box.AppendText(str);
                    text_box.SelectionColor = text_box.ForeColor;
                });
            };
            string settings_path = this.errorRichTextBox.Text;
            worker.DoWork += (object se, DoWorkEventArgs ev) =>
            {
                Analyze.Run(dirs, settings, report_log, error_log, ((BackgroundWorker)se).ReportProgress);
            };
            this.executeButton.Enabled = false;
            worker.RunWorkerAsync();
        }

        private void open_folder_dialog_and_save_path(Control save_in)
        {
            CommonOpenFileDialog folder_dialog = new CommonOpenFileDialog();
            // dialog parameters
            folder_dialog.IsFolderPicker = true;
            folder_dialog.InitialDirectory = Environment.CurrentDirectory;
            folder_dialog.Multiselect = false;
            if (folder_dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                save_in.Text = folder_dialog.FileName;
            }
        }

        private void open_file_dialog_and_save_path(Control save_in, string filter)
        {
            OpenFileDialog file_dialog = new OpenFileDialog();
            // dialog parameters
            file_dialog.InitialDirectory = Environment.CurrentDirectory;
            file_dialog.RestoreDirectory = true;
            file_dialog.Filter = filter;
            file_dialog.FilterIndex = 1;
            file_dialog.Multiselect = false;
            file_dialog.CheckPathExists = file_dialog.CheckFileExists = true;
            if (file_dialog.ShowDialog() == DialogResult.OK)
            {
                save_in.Text = file_dialog.FileName;
            }
        }

        private void browseLocationButton_Click(object sender, EventArgs e)
        {
            open_folder_dialog_and_save_path(this.locationTextBox);
        }

        private void browseSettingsButton_Click(object sender, EventArgs e)
        {
            open_file_dialog_and_save_path(this.settingsTextBox, "Pliki json (*.json)|*.json|Wszystkie pliki (*.*)|*.*");
        }

        private void reportRichTextBox_TextChanged(object sender, EventArgs e)
        {
            this.reportRichTextBox.ScrollToCaret();
        }

        private void errorRichTextBox_TextChanged(object sender, EventArgs e)
        {
            this.errorRichTextBox.ScrollToCaret();
        }
    }
}
