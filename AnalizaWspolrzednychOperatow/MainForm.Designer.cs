﻿namespace AnalizaWspolrzednychOperatow
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.locationTextBox = new System.Windows.Forms.TextBox();
            this.browseLocationButton = new System.Windows.Forms.Button();
            this.locationGroupBox = new System.Windows.Forms.GroupBox();
            this.recursiveCheckBox = new System.Windows.Forms.CheckBox();
            this.executeButton = new System.Windows.Forms.Button();
            this.reportRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.errorRichTextBox = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.clearReportButton = new System.Windows.Forms.Button();
            this.clearErrorButton = new System.Windows.Forms.Button();
            this.settingsCheckBox = new System.Windows.Forms.GroupBox();
            this.settingsTextBox = new System.Windows.Forms.TextBox();
            this.browseSettingsButton = new System.Windows.Forms.Button();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.locationGroupBox.SuspendLayout();
            this.settingsCheckBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // locationTextBox
            // 
            this.locationTextBox.Location = new System.Drawing.Point(6, 18);
            this.locationTextBox.Name = "locationTextBox";
            this.locationTextBox.ReadOnly = true;
            this.locationTextBox.Size = new System.Drawing.Size(572, 20);
            this.locationTextBox.TabIndex = 0;
            // 
            // browseLocationButton
            // 
            this.browseLocationButton.Location = new System.Drawing.Point(584, 17);
            this.browseLocationButton.Name = "browseLocationButton";
            this.browseLocationButton.Size = new System.Drawing.Size(25, 20);
            this.browseLocationButton.TabIndex = 1;
            this.browseLocationButton.Text = "...";
            this.browseLocationButton.UseVisualStyleBackColor = true;
            this.browseLocationButton.Click += new System.EventHandler(this.browseLocationButton_Click);
            // 
            // locationGroupBox
            // 
            this.locationGroupBox.Controls.Add(this.recursiveCheckBox);
            this.locationGroupBox.Controls.Add(this.locationTextBox);
            this.locationGroupBox.Controls.Add(this.browseLocationButton);
            this.locationGroupBox.Location = new System.Drawing.Point(12, 12);
            this.locationGroupBox.Name = "locationGroupBox";
            this.locationGroupBox.Size = new System.Drawing.Size(615, 68);
            this.locationGroupBox.TabIndex = 2;
            this.locationGroupBox.TabStop = false;
            this.locationGroupBox.Text = "Lokalizacja";
            // 
            // recursiveCheckBox
            // 
            this.recursiveCheckBox.AutoSize = true;
            this.recursiveCheckBox.Checked = true;
            this.recursiveCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.recursiveCheckBox.Location = new System.Drawing.Point(6, 45);
            this.recursiveCheckBox.Name = "recursiveCheckBox";
            this.recursiveCheckBox.Size = new System.Drawing.Size(233, 17);
            this.recursiveCheckBox.TabIndex = 2;
            this.recursiveCheckBox.Text = "Wyszukiwanie rekursywne (w podfolderach)";
            this.recursiveCheckBox.UseVisualStyleBackColor = true;
            // 
            // executeButton
            // 
            this.executeButton.Font = new System.Drawing.Font("MisterEarl BT", 27.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.executeButton.Image = ((System.Drawing.Image)(resources.GetObject("executeButton.Image")));
            this.executeButton.Location = new System.Drawing.Point(633, 12);
            this.executeButton.Name = "executeButton";
            this.executeButton.Size = new System.Drawing.Size(155, 124);
            this.executeButton.TabIndex = 3;
            this.executeButton.UseVisualStyleBackColor = true;
            this.executeButton.Click += new System.EventHandler(this.executeButton_Click);
            // 
            // reportRichTextBox
            // 
            this.reportRichTextBox.Location = new System.Drawing.Point(12, 155);
            this.reportRichTextBox.Name = "reportRichTextBox";
            this.reportRichTextBox.ReadOnly = true;
            this.reportRichTextBox.Size = new System.Drawing.Size(776, 330);
            this.reportRichTextBox.TabIndex = 4;
            this.reportRichTextBox.Text = "";
            this.reportRichTextBox.TextChanged += new System.EventHandler(this.reportRichTextBox_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 139);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Raport";
            // 
            // errorRichTextBox
            // 
            this.errorRichTextBox.Location = new System.Drawing.Point(12, 544);
            this.errorRichTextBox.Name = "errorRichTextBox";
            this.errorRichTextBox.ReadOnly = true;
            this.errorRichTextBox.Size = new System.Drawing.Size(776, 159);
            this.errorRichTextBox.TabIndex = 6;
            this.errorRichTextBox.Text = "";
            this.errorRichTextBox.TextChanged += new System.EventHandler(this.errorRichTextBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 528);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Błędy";
            // 
            // clearReportButton
            // 
            this.clearReportButton.Location = new System.Drawing.Point(713, 491);
            this.clearReportButton.Name = "clearReportButton";
            this.clearReportButton.Size = new System.Drawing.Size(75, 27);
            this.clearReportButton.TabIndex = 8;
            this.clearReportButton.Text = "Wyczyść";
            this.clearReportButton.UseVisualStyleBackColor = true;
            // 
            // clearErrorButton
            // 
            this.clearErrorButton.Location = new System.Drawing.Point(713, 709);
            this.clearErrorButton.Name = "clearErrorButton";
            this.clearErrorButton.Size = new System.Drawing.Size(75, 27);
            this.clearErrorButton.TabIndex = 9;
            this.clearErrorButton.Text = "Wyczyść";
            this.clearErrorButton.UseVisualStyleBackColor = true;
            // 
            // settingsCheckBox
            // 
            this.settingsCheckBox.Controls.Add(this.settingsTextBox);
            this.settingsCheckBox.Controls.Add(this.browseSettingsButton);
            this.settingsCheckBox.Location = new System.Drawing.Point(12, 86);
            this.settingsCheckBox.Name = "settingsCheckBox";
            this.settingsCheckBox.Size = new System.Drawing.Size(615, 50);
            this.settingsCheckBox.TabIndex = 10;
            this.settingsCheckBox.TabStop = false;
            this.settingsCheckBox.Text = "Opcje";
            // 
            // settingsTextBox
            // 
            this.settingsTextBox.Location = new System.Drawing.Point(6, 19);
            this.settingsTextBox.Name = "settingsTextBox";
            this.settingsTextBox.ReadOnly = true;
            this.settingsTextBox.Size = new System.Drawing.Size(572, 20);
            this.settingsTextBox.TabIndex = 3;
            // 
            // browseSettingsButton
            // 
            this.browseSettingsButton.Location = new System.Drawing.Point(584, 18);
            this.browseSettingsButton.Name = "browseSettingsButton";
            this.browseSettingsButton.Size = new System.Drawing.Size(25, 20);
            this.browseSettingsButton.TabIndex = 4;
            this.browseSettingsButton.Text = "...";
            this.browseSettingsButton.UseVisualStyleBackColor = true;
            this.browseSettingsButton.Click += new System.EventHandler(this.browseSettingsButton_Click);
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 747);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(776, 23);
            this.progressBar.TabIndex = 11;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 782);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.settingsCheckBox);
            this.Controls.Add(this.clearErrorButton);
            this.Controls.Add(this.clearReportButton);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.errorRichTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.reportRichTextBox);
            this.Controls.Add(this.executeButton);
            this.Controls.Add(this.locationGroupBox);
            this.Name = "MainForm";
            this.Text = "Analiza Excel";
            this.locationGroupBox.ResumeLayout(false);
            this.locationGroupBox.PerformLayout();
            this.settingsCheckBox.ResumeLayout(false);
            this.settingsCheckBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox locationTextBox;
        private System.Windows.Forms.Button browseLocationButton;
        private System.Windows.Forms.GroupBox locationGroupBox;
        private System.Windows.Forms.CheckBox recursiveCheckBox;
        private System.Windows.Forms.Button executeButton;
        private System.Windows.Forms.RichTextBox reportRichTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox errorRichTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button clearReportButton;
        private System.Windows.Forms.Button clearErrorButton;
        private System.Windows.Forms.GroupBox settingsCheckBox;
        private System.Windows.Forms.TextBox settingsTextBox;
        private System.Windows.Forms.Button browseSettingsButton;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}

